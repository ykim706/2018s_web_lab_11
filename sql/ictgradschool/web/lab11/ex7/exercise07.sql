-- Answers to Exercise 7 here
DROP TABLE IF EXISTS comments;
CREATE TABLE IF NOT EXISTS comments(
commentid INT NOT NULL  AUTO_INCREMENT,
articleid INT NOT NULL,
comment VARCHAR(1000),
PRIMARY KEY(commentid),
FOREIGN KEY (articleid) REFERENCES articles(id)
);

INSERT INTO comments (articleid, comment) VALUES
(1, 'It explains what Lorem Ipsum is.'),
(2, 'It explains where Lorem Ipsum comes from.');

