-- Answers to Exercise 2 here
DROP TABLE IF EXISTS tableone;
CREATE TABLE IF NOT EXISTS tableone(
username VARCHAR(20) NOT NULL,
firstname VARCHAR(20),
lastname VARCHAR (20),
email VARCHAR (50)
);

INSERT INTO tableone (username, firstname, lastname, email) VALUES
('programmer1','Bill','Gates', 'bill@microsoft.com'),
('programmer2', 'Peter', 'Liu','liu@microsoft.com'),
('programmer3', 'Pete', 'Kim', 'kim@microsoft.com'),
('programmer4','Lawrence' , 'Peterson','peterson@microsoft.com');

