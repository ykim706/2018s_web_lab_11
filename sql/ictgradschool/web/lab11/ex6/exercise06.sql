-- Answers to Exercise 6 here
DROP TABLE IF EXISTS movies;
CREATE TABLE IF NOT EXISTS movies(
fullname VARCHAR(50) NOT NULL,
id INT NOT NULL,
title VARCHAR(1000),
director VARCHAR(30),
weeklychargerate INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (fullname) REFERENCES customers (fullname)
);

INSERT INTO movies(fullname, id, title, director, weeklychargerate ) VALUES
('Peter Jackson', '1111', 'Bumblebee', 'Travis Knight', '2'),
('Jane Campion', '2222', 'Bumblebee2', 'Travis Knight', '6'),
('Russell Crowe', '3333', 'Ralph Breaks the Internet: Wreck-It Ralph 2', 'Phil Johnston', '4' );