-- Answers to Exercise 5 here
DROP TABLE IF EXISTS tabletwo;
CREATE TABLE IF NOT EXISTS tabletwo(
username VARCHAR(20) NOT NULL,
firstname VARCHAR(20),
lastname VARCHAR (20),
email VARCHAR (50),
PRIMARY KEY(username)
);

INSERT INTO tabletwo (username, firstname, lastname, email) VALUES
('programmer1','Bill','Gates', 'bill@microsoft.com'),
('programmer2', 'Peter', 'Liu','liu@microsoft.com'),
('programmer3', 'Pete', 'Kim', 'kim@microsoft.com'),
('programmer4','Lawrence' , 'Peterson','peterson@microsoft.com');

