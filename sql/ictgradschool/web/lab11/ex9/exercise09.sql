-- Answers to Exercise 9 here

SELECT * FROM customers;

SELECT fullname, gender, yearborn, yearjoined FROM customers;

SELECT title FROM articles;

SELECT DISTINCT director FROM movies;

SELECT title FROM movies WHERE weeklychargerate <= 2;

SELECT username FROM tableone ORDER BY username;

SELECT username FROM tableone WHERE firstname LIKE 'Pete%';

SELECT username FROM tableone WHERE firstname LIKE 'Pete%' OR lastname LIKE 'Pete%';

